<?php
return array(
    'form/show/([0-9]+)'=> 'task/show/$1',
    'edit/task/([0-9]+)' =>  'task/edit/$1',
    'add/new-task' =>  'task/add',
    'login' =>  'task/login',
    'admin' => 'task/admin',
    '' =>  'task/task'

);