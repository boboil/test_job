<?php

class User
{

    public static function checkUserData($name, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM users WHERE name = :name AND password = :password';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_INT);
        $result->bindParam(':password', $password, PDO::PARAM_INT);
        $result->execute();

        $user = $result->fetch();
        if ($user) {

            return $user['id'];
        }

        return false;
    }
    public static function auth($userId)
    {
        session_start();
        $_SESSION['user'] = $userId;
    }
    public static function checkAdmin()
    {
        session_start();
        if ($_SESSION['user'] == 1)
        {
            return 'admin';
        }
        return 'guest';
    }
}

?>