<?php
include_once ROOT. '/components/DB.php';
class Task{

    public static function getTasks()
    {
        $db = DB::getConnection();

        $tasks = [];
        $result = $db->query("SELECT * FROM `tasks`");
        $i = 0;
        while ($row = $result->fetch()){
            $tasks[$i]['id'] = $row['id'];
            $tasks[$i]['user_id'] = $row['user_id'];
            $tasks[$i]['text'] = $row['text'];
            $tasks[$i]['email'] = $row['author_email'];
            $tasks[$i]['name'] = $row['author_name'];
            $tasks[$i]['moderated'] = $row['moderated'];
            $i++;
        }
        return $tasks;
    }
    public static function getSingleTask($id)
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM `tasks` WHERE `id` = $id");
        $task = $result->fetch();
        return $task;
    }
    public static function user()
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM tasks LEFT JOIN users ON tasks.user_id = users.id");
        $user = $result->fetch();
        return $user;
    }
    public static function addNewTask($data)
    {
        $user = 1;
        $text = $data['text'];
        $author =  $data['name'];
        $mail =  $data['email'];
        $db = DB::getConnection();
        $result = $db->query("INSERT INTO `tasks` (`user_id`, `text`, `author_name`, `author_email`) VALUES ('$user', '$text', '$author', '$mail');");
        return true;
    }
    public static function editTask($data, $id)
    {
        $user_id = 1;
        $text = $data['text'];
        $author =  $data['name'];
        $mail =  $data['email'];
        if ($data['moderated'] == 'on'){
            $moderated = 1;
        }else{
            $moderated = 0;
        }

        $db = DB::getConnection();
        $sql = "UPDATE tasks
            SET 
                user_id = :user_id, 
                text = :text, 
                author_name = :author_name, 
                author_email = :author_email, 
                moderated = :moderated
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $result->bindParam(':author_name', $author, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':author_email', $mail, PDO::PARAM_STR);
        $result->bindParam(':moderated', $moderated, PDO::PARAM_BOOL);

        return $result->execute();
    }
}