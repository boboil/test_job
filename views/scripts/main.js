$(function() {
    $("#form_add").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = '/add/new-task';
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                alert('добавлено');
            }
        });
    });
    $("#form_edit").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var  id = $('#id').val();
        var url = '/edit/task/' + id;
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                alert('отредактировано');
                location.reload();
            }
        });
    });
    $("#login_form").submit(function(e) {
        e.preventDefault();
        var form = $(this);
        var url = '/login';
        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(),
            success: function(data)
            {
                window.location.replace('/admin')
            }
        });
    });
});