<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="col-lg-12">
        <a href="/add/new-task">Добавить новую задачу</a><br/>
        <?php if ($admin == 'guest') { ?>
        <a href="/login">Войти</a>
        <?php } ?>
        <table id="table_id" class="display" data-page-length='3'>
            <thead>
            <tr>
                <th>Имя пользователя</th>
                <th>Email</th>
                <th>текста задачи</th>
                <?php if ($admin == 'admin') { ?>
                    <th> Редактировать</th>
                <?php } ?>
                <th>выполнено</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($tasks as $task) { ?>
            <tr>
                <td><?php echo $task['name'] ?></td>
                <td><?php echo $task['email'] ?></td>
                <td><?php echo $task['text'] ?></td>
                <?php if ($admin == 'admin') { ?>
                    <td><a href="/form/show/<?php echo $task['id'] ?>">редактировать</a></td>
                <?php } ?>
                <?php if ($task['moderated'] == 1) { ?>
                    <td>YES</td>
                <?php }else { ?>
                    <td>No</td>
                <?php } ?>
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<footer>
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="/views/scripts/main.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
</footer>
</body>
</html>