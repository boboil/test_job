<?php


include_once ROOT. '/model/Task.php';
include_once ROOT. '/model/User.php';

class TaskController
{
    public function task()
    {
        $tasks =  Task::getTasks();
        $admin = User::checkAdmin();
        require_once (ROOT .'/views/index.php');
        return true;
    }
    public function show($id)
    {
        $task = Task::getSingleTask($id[0]);
        require_once (ROOT .'/views/single.php');
        return true;
    }
    public function edit($id)
    {
        if (isset($_POST) && !empty($_POST))
        {
            $data = [
                'text'=>$_POST['text'],
                'name'=>$_POST['name'],
                'email'=>$_POST['email'],
                'moderated'=>$_POST['moderated'],
            ];
            Task::editTask($data, $id[0]);
        }
        $task = Task::getSingleTask($id[0]);
        require_once (ROOT .'/views/single.php');
    }
    public function add()
    {
        if (isset($_POST) && !empty($_POST))
        {
            $data = [
                'text'=>$_POST['text'],
                'name'=>$_POST['name'],
                'email'=>$_POST['email'],
            ];
            Task::addNewTask($data);
        }
        require_once (ROOT .'/views/single.php');
    }
    public function login()
    {
        $name = '';
        $password = '';
        if (isset($_POST) && !empty($_POST))
        {
            $name = $_POST['name'];
            $password = $_POST['password'];
        }
        $userId = User::checkUserData($name, $password);
        if ($userId == false) {
            $errors[] = 'Неправильные данные для входа на сайт';
         } else {
            User::auth($userId);
        }
        require_once (ROOT .'/views/login.php');

        return true;
    }
    public function admin()
    {
        $tasks =  Task::getTasks();
        $admin = User::checkAdmin();
        require_once (ROOT .'/views/index.php');
        return true;
    }
}